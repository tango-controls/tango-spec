#!/bin/sh
prefix=/usr/local
sql_dir=$prefix/share/tango/db

mysql=/usr/bin/mysql
mysql_config=/etc/sysconfig/tango-db
mysql_host=localhost
mysql_admin_user=root
mysql_admin_passwd=
mysql_tango_user=tango
mysql_tango_passwd=tango
db_name=tango

echo -n "MySQL host name [$mysql_host]:"
read answer
[[ -n $answer ]] && mysql_host=$answer

echo -n "MySQL admin user name [$mysql_admin_user]:"
read answer
[[ -n $answer ]] && mysql_admin_user=$answer

echo -n "MySQL admin password [$mysql_admin_passwd]:"
read answer
[[ -n $answer ]] && mysql_admin_passwd=$answer

echo -n "Name of tango MySQL user [$mysql_tango_user]:"
read answer
[[ -n $answer ]] && mysql_tango_user=$answer

echo -n "Password for tango MySQL user [$mysql_tango_passwd]:"
read answer
[[ -n $answer ]] && mysql_tango_passwd=$answer
 
if test "x$mysql_host" = "x"; then
    host_switch="";
else
    host_switch="-h$mysql_host";
fi

if test "x$mysql_admin_user" = "x"; then
    user_switch="";
else
    user_switch="-u$mysql_admin_user";
fi

if test "x$mysql_admin_passwd" = "x"; then
    passwd_switch="";
else
    passwd_switch="-p$mysql_admin_passwd"
fi

connect="$mysql $host_switch $user_switch $passwd_switch" 

if `echo quit | $connect $db_name >/dev/null 2>&1`; then
   if test "x$mysql_host" = "x"; then
      echo "The $db_name database is already defined on this host"
   else
      echo "The $db_name database is already defined on $mysql_host"
   fi
   echo "Please run "
   echo "$prefix/update_db.sh"
   exit 1
fi

$connect < $sql_dir/create_db.sql

[[ $? -eq 0 ]] || exit $?
echo
echo "Created database '$db_name'"

cat << EOF | $connect
CREATE USER '$mysql_tango_user'@'$mysql_host'
IDENTIFIED BY '$mysql_tango_passwd';
GRANT ALL PRIVILEGES ON ${db_name}.* TO '$mysql_tango_user'@'$mysql_host'
WITH GRANT OPTION;
EOF

[[ $? -eq 0 ]] || exit $?
echo
echo "Created user '$mysql_tango_user'"

if [ `id -u` -eq 0 ]; then
    echo "MYSQL_USER=$mysql_tango_user" > $mysql_config
    echo "MYSQL_PASSWORD=$mysql_tango_passwd" >> $mysql_config
    echo
    echo "Wrote  $mysql_config"
else
    echo
    echo "You don't have permission to write $mysql_config:"
    echo "As root, add the following:"
    echo "MYSQL_USER=$mysql_tango_user"
    echo "MYSQL_PASSWORD=$mysql_tango_passwd"
fi

exit 0
